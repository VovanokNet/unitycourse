﻿using UnityEngine;

public abstract class ClickCatcher : MonoBehaviour
{
	public abstract void ClickCatch(RaycastHit hit);
}
