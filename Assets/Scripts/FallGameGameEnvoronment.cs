﻿using UnityEngine;
using System.Collections.Generic;

public class FallGameGameEnvoronment : MonoBehaviour
{
	[SerializeField] private Transform cellsContainer;
	[SerializeField] private List<GameObject> cellsPrefabs;
	[SerializeField] private Vector3 polySize;
	[SerializeField] private Transform playerInitialPosition;
	[SerializeField] private GameObject playerPrefab;
	[SerializeField] private Transform exitInitialPosition;
	[SerializeField] private GameObject exitPrefab;

	private void Start()
	{
		for (int x = 0; x < polySize.x; x++)
		{
			for (int y = 0; y < polySize.y; y++)
			{
				for (int z = 0; z < polySize.z; z++)
				{
					int cellPrefabIndex = Random.Range(0, cellsPrefabs.Count);
					GameObject newCellGo = Instantiate(cellsPrefabs[cellPrefabIndex], new Vector3(x, y, z), Quaternion.identity) as GameObject;
					newCellGo.transform.SetParent(cellsContainer);
				}
			}
		}

		cellsContainer.position = -polySize / 2;

		//Init player
		Instantiate(playerPrefab, playerInitialPosition.position, Quaternion.identity);

		//Init exit
		GameObject exitGo = Instantiate(exitPrefab);
		exitGo.transform.position = exitInitialPosition.position;
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			Ray rayFromCamera = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hitInfo;
			if (Physics.Raycast(rayFromCamera, out hitInfo))
			{
				if (hitInfo.transform.GetComponent<Cell>() != null)
				{
					Destroy(hitInfo.transform.gameObject);
				}
			}
		}
	}
}
