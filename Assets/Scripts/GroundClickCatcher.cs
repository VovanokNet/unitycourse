﻿using UnityEngine;

public class GroundClickCatcher : ClickCatcher
{
	[SerializeField] private GameObject aimPrefab;

	private Aim aim;

	private void Start()
	{
		var aimGo = Instantiate(aimPrefab);
		aim = aimGo.GetComponent<Aim>();
	}

	public override void ClickCatch(RaycastHit hit)
	{
	//	var go = GameObject.CreatePrimitive(PrimitiveType.Cylinder);
	//	go.transform.position = hit.point;
	//	go.GetComponent<Renderer>().material.color = Color.green;
	//	go.AddComponent<Rigidbody>();
	//	go.AddComponent<CillinderClickCatcher>();

		aim.transform.position = hit.point;
		aim.Activate();
	}
}
