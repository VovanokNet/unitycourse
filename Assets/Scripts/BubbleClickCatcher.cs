﻿using System;
using UnityEngine;

public class BubbleClickCatcher : ClickCatcher
{
	public override void ClickCatch(RaycastHit hit)
	{
		GetComponent<Renderer>().material.color = Color.blue;
	}
}
