﻿using System;
using UnityEngine;
using System.Collections;

public class GameEnvironment : MonoBehaviour
{
	[SerializeField] private int countBubble;
	[SerializeField] private float createBubbleDelteTime;
	[SerializeField] private GameObject bubblePrefab;
	[SerializeField] private float minX;
	[SerializeField] private float maxX;
	[SerializeField] private float minZ;
	[SerializeField] private float maxZ;
	[SerializeField] private float y;

	private Timer bubbleSpawnTimer;

	private void Start()
	{
		//	GameObject goVariant1 = new GameObject("GoVariant1");
		//	goVariant1.transform.parent = transform;
		//	goVariant1.AddComponent<Rigidbody>();

		//	var goVariant2 = GameObject.CreatePrimitive(PrimitiveType.Sphere);
		//	goVariant2.transform.parent = transform;

		//for (int i = 0; i < countBubble; i++)
		//{
		//	CreateOneRainBubble();
		//}

		//GameObject.CreatePrimitive(PrimitiveType.Sphere)

		bubbleSpawnTimer = GetComponent<Timer>();
		bubbleSpawnTimer.OnTimeElapse += BubbleSpawnTimerOnOnTimeElapse;
	}

	private void Update()
	{
		if (Input.GetMouseButtonDown(0))
		{
			var cameraRay = Camera.main.ScreenPointToRay(Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast(cameraRay, out hit))
			{
				var clickCatcher = hit.transform.gameObject.GetComponent<ClickCatcher>();
				if (clickCatcher != null)
				{
					clickCatcher.ClickCatch(hit);
				}
			}
		}

		if (Input.GetMouseButton(1))
		{
			Debug.Log("RIGHT BUTTON");
		}

		if (Input.GetMouseButton(3))
		{
			Debug.Log("MID BUTTON");
		}

		if (Input.GetButtonDown("Jump"))
		{
			Debug.Log("JUMP");
		}

		if (Input.GetKey(KeyCode.A))
		{
			Debug.Log("A");
		}
	}

	private void BubbleSpawnTimerOnOnTimeElapse(object sender, EventArgs eventArgs)
	{
		CreateOneRainBubble();
	}

	private void CreateOneRainBubble()
	{
		Vector3 initialPosition = new Vector3(UnityEngine.Random.Range(minX, maxX), y, UnityEngine.Random.Range(minZ, maxZ));
		GameObject newBubble = Instantiate(bubblePrefab, initialPosition, Quaternion.identity) as GameObject;
		newBubble.transform.parent = transform;
		newBubble.GetComponent<Bubble>().OnFallToAbyss += BubbleFallToAbyss;
	}

	private void BubbleFallToAbyss(object sender, Bubble.FallToAbyssEventArgs args)
	{
		args.FalledBubble.OnFallToAbyss -= BubbleFallToAbyss;

		Destroy(args.FalledBubble.gameObject);
	}

	private void OnDestroy()
	{
		bubbleSpawnTimer.OnTimeElapse -= BubbleSpawnTimerOnOnTimeElapse;
	}
}