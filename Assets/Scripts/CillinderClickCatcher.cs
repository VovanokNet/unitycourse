﻿using UnityEngine;

public class CillinderClickCatcher : ClickCatcher
{
	public override void ClickCatch(RaycastHit hit)
	{
		GetComponent<Rigidbody>().AddForce(Vector3.up * 10, ForceMode.Impulse);
	}
}
