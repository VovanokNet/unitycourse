﻿using System;
using UnityEngine;

public class Timer : MonoBehaviour
{
	[SerializeField] private float deltaTime;

	public event EventHandler OnTimeElapse;

	private float lastTimeDispatch;

	private void Start()
	{
		Reset();
	}

	private void Update()
	{
		if (Time.time - lastTimeDispatch >= deltaTime)
		{
			lastTimeDispatch = Time.time;

			if (OnTimeElapse != null)
			{
				OnTimeElapse(this, EventArgs.Empty);
			}
		}
	}

	public void Reset()
	{
		lastTimeDispatch = Time.time;
	}
}
