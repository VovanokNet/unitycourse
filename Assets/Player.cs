﻿using UnityEngine;

public class Player : MonoBehaviour
{
	private void OnCollisionEnter(Collision collision)
	{
		if (collision.gameObject.CompareTag("Abyss"))
		{
			Debug.Log("Проиграли!");
			Destroy(gameObject);
			return;
		}

		if (collision.gameObject.CompareTag("Exit"))
		{
			Debug.Log("Выиграли!");
			Destroy(gameObject);
		}
	}
}
