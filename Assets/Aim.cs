﻿using System;
using UnityEngine;

public class Aim : MonoBehaviour
{
	[SerializeField] private float fadeSpeed;
	[SerializeField] private AnimationCurve alphaChangeCurve;

	private Timer timerBeforeFade;
	private bool isFade;
	private Renderer selfRenderer;
	private float fadeValue;
	
	private void Start()
	{
		timerBeforeFade = GetComponent<Timer>();
		selfRenderer = GetComponent<Renderer>();

		Deactivate();
	}

	private void TimerBeforeFadeOnOnTimeElapse(object sender, EventArgs eventArgs)
	{
		timerBeforeFade.OnTimeElapse -= TimerBeforeFadeOnOnTimeElapse;

		isFade = true;
	}

	private void Update()
	{
		if (isFade)
		{
			fadeValue += fadeSpeed * Time.deltaTime;

			var bufferedColor = selfRenderer.material.GetColor("_Color");
			bufferedColor.a = alphaChangeCurve.Evaluate(Mathf.Clamp01(fadeValue));
			selfRenderer.material.SetColor("_Color", bufferedColor);

			if (fadeValue >= 1f)
			{
				gameObject.SetActive(false);
			}
		}
	}

	public void Activate()
	{
		gameObject.SetActive(true);

		isFade = false;

		timerBeforeFade.OnTimeElapse += TimerBeforeFadeOnOnTimeElapse;
		timerBeforeFade.Reset();

		var bufferedColor = selfRenderer.material.GetColor("_Color");
		bufferedColor.a = 1.0f;
		selfRenderer.material.SetColor("_Color", bufferedColor);

		fadeValue = 0f;
	}

	private void Deactivate()
	{
		gameObject.SetActive(false);
	}
}
